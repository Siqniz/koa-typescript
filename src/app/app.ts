import * as Koa from 'koa';
import movieController from '../movie/movie.controller';

const app:Koa = new Koa;

app.use(movieController.routes());
app.use(movieController.allowedMethods());

app.use(async (ctx:Koa.Context) =>{
    ctx.body="Hello World Koa ts server"
});

export default app;